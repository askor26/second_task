import React from 'react';
import {
  SearchOutlined,
  PlusCircleOutlined,
  FilterOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import {Input, Space, Button} from 'antd';

const SearchBar = () => {
  return (
    <Space className="justify-between m-5">
      <Input
        placeholder="Поиск"
        prefix={<SearchOutlined className="text-purple-500" />}
      />
      <Space size={0}>
        <Button
          icon={
            <PlusCircleOutlined
              style={{
                verticalAlign: 'middle',
              }}
            />
          }
        >
          Добавить
        </Button>
        <Button
          icon={
            <FilterOutlined
              style={{
                verticalAlign: 'middle',
              }}
            />
          }
        >
          Фильтр
        </Button>
        <Button
          icon={
            <CloudUploadOutlined
              style={{
                verticalAlign: 'middle',
              }}
            />
          }
        >
          Экспорт
        </Button>
      </Space>
    </Space>
  );
};

export default SearchBar;
