import React, {useState} from 'react';
import {StarTwoTone} from '@ant-design/icons';
import {Table, Image, Space, Checkbox} from 'antd';
import type {ColumnsType, TablePaginationConfig} from 'antd/es/table';
import type {FilterValue, SorterResult} from 'antd/es/table/interface';

interface DataType {
  key: string;
  rating: number;
  name: string;
  placeName: string;
  picture: HTMLElement;
  city: string;
  address: string;
  available: HTMLElement;
}
interface TableParams {
  pagination?: TablePaginationConfig;
  sortField?: string;
  sortOrder?: string;
  filters?: Record<string, FilterValue>;
}
const columns: ColumnsType<DataType> = [
  {
    title: 'Рейтинг',
    dataIndex: 'rating',
    align: 'left',
    width: '6rem',
    render: () => (
      <Space align="center">
        <StarTwoTone
          className="p-0"
          style={{
            verticalAlign: 'middle',
          }}
        />
        <div>4</div>
      </Space>
    ),
  },
  {
    title: 'Название заведения',
    dataIndex: 'placeName',
    align: 'left',
    sorter: true,
  },
  {
    title: 'Обложка',
    dataIndex: 'picture',
    align: 'center',
    width: '10rem',
    render: () => (
      <Image
        width={50}
        preview={false}
        className="rounded-lg"
        src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
      />
    ),
  },
  {
    title: 'Город',
    dataIndex: 'city',
    align: 'left',
    sorter: true,
  },
  {
    title: 'Адрес заведения',
    dataIndex: 'address',
    align: 'left',
    sorter: true,
  },
  {
    title: 'Доступно',
    dataIndex: 'available',
    align: 'center',
    width: '8rem',
    render: () => <Checkbox />,
  },
];
let key = 0;
const incrementKey = () => {
  key += 1;
  return key;
};
const getTableData = () => {
  return {
    key: incrementKey(),
    rating: 4,
    name: 'Name',
    placeName: 'Place name',
    city: 'City',
    address: 'Adress',
    picture: 'Picture',
    available: 'Available',
  };
};

const data: DataType[] = Array(50).fill(getTableData());

const TableElem = () => {
  const [tableParams, setTableParams] = useState<TableParams>({
    pagination: {
      current: 1,
      pageSize: 10,
      showSizeChanger: true,
      position: ['bottomLeft'],
    },
  });
  const handleTableChange = (
    pagination: TablePaginationConfig,
    filters: Record<string, FilterValue>,
    sorter: SorterResult<DataType>
  ) => {
    setTableParams({
      pagination,
      filters,
      ...sorter,
    });
  };

  return (
    <>
      <Table
        className="m-5 my-0 mb-10 rounded-lg"
        columns={columns}
        dataSource={data}
        bordered
        scroll={{y: 740}}
        pagination={tableParams.pagination}
        onChange={handleTableChange}
      />
    </>
  );
};

export default TableElem;
