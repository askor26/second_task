import React from 'react';
import {MailOutlined} from '@ant-design/icons';
import type {MenuProps} from 'antd';
import {Menu} from 'antd';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group'
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}
let key = 0;
const incrementKey = () => {
  key += 1;
  return key;
};
const items: MenuProps['items'] = [
  getItem(
    'Заведения',
    'grp1',
    null,
    [
      getItem(
        'Заведения',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Сотрудники',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
    ],
    'group'
  ),
  getItem(
    'Маркетинг',
    'grp2',
    null,
    [
      getItem(
        'Новости и акции',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Электронные карты',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Push-рассылки',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Промокоды',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
    ],
    'group'
  ),
  getItem(
    'Клиенты',
    'grp3',
    null,
    [
      getItem(
        'Клиенты',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Уровни',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Операции',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Сгорающие бонусы',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Сгорающие бонусы',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Оценки и отзывы',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Безопасность',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
    ],
    'group'
  ),
  getItem(
    'Меню',
    'grp4',
    null,
    [
      getItem(
        'Категории товаров',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
      getItem(
        'Товары и услуги',
        incrementKey(),
        <MailOutlined style={{color: '#4c1d95'}} />
      ),
    ],
    'group'
  ),
];

const MenuElem: React.FC = () => {
  return <Menu mode="inline" items={items} theme="light" />;
};

export default MenuElem;
