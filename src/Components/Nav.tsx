import React, {useState} from 'react';
import {MenuUnfoldOutlined, CreditCardOutlined} from '@ant-design/icons';
import {Layout, Space, Button, Image} from 'antd';
import mainLogo from '../images/MainLogo.png';
const {Header} = Layout;

const Nav = () => {
  const [collapsed, setCollapsed] = useState(false);

  const CollapseBtn = () => {
    return (
      <MenuUnfoldOutlined
        style={{
          verticalAlign: 'middle',
        }}
        className="trigger"
        onClick={() => {
          setCollapsed(!collapsed);
        }}
      />
    );
  };
  const UserElem = () => {
    return (
      <Space>
        <Image
          preview={false}
          src={mainLogo}
          className="object-scale-down"
          width={60}
        />
        <Space direction="vertical" size={0}>
          <div className="font-medium">Demo User</div>
          <div className="">demo@exmaple.com</div>
        </Space>
      </Space>
    );
  };
  return (
    <Header className="bg-white header leading-5  pl-5 flex justify-between border-b">
      <Space align="center">
        <CollapseBtn />
        <p className="font-medium text-base">Заведения</p>
      </Space>

      <Space className="">
        <Button className="bg-purple-500 text-white">Сообщения</Button>
        <Space className="text-green-600 m-4">
          <CreditCardOutlined
            style={{
              verticalAlign: 'middle',
            }}
          />
          <p>Баланс: 7000 Р</p>
        </Space>
        <UserElem />
      </Space>
    </Header>
  );
};

export default Nav;
