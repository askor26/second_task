import React from 'react';
import {Layout, Image, Space} from 'antd';
import mainLogo from '../images/MainLogo.png';
import MenuElem from './Menu';
const {Sider} = Layout;

const SiderBar = () => {
  return (
    <Sider className="h-screen " style={{background: '#FFFFFF'}}>
      <Space
        align="center"
        style={{width: '100%', justifyContent: 'center'}}
        className="bg-white justify-content-center p-10"
      >
        <Image
          src={mainLogo}
          preview={false}
          className="object-scale-down "
          width={150}
        />
      </Space>
      <MenuElem />
    </Sider>
  );
};

export default SiderBar;
