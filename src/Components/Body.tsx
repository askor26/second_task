import React from 'react';
import {Layout} from 'antd';

import SiderBar from './SiderBar';
import Nav from './Nav';
import TableElem from './Table';
import SearchBar from './SearchBar';

const Body: React.FC = () => {
  return (
    <Layout className="bg-white">
      <SiderBar />
      <Layout className="site-layout bg-white ">
        <Nav />
        <SearchBar />
        <TableElem />
      </Layout>
    </Layout>
  );
};
export default Body;
