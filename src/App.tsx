import React from 'react';
import './App.css';

import Body from './Components/Body';
function App() {
  return (
    <div className="h-screen">
      <Body />
    </div>
  );
}

export default App;
